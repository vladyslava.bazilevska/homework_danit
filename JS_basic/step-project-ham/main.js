// <!-- Our Services -->

document.querySelector('.service-list-title').addEventListener('click', (event) => {
    document.querySelector('.service-item-title.active-service-item-title').classList.remove('active-service-item-title');
    event.target.classList.add('active-service-item-title');
    document.querySelector('.service-item-article.active-service-item-article').classList.remove('active-service-item-article');
    document.querySelector(`.service-list-article li[data-tab = "${event.target.dataset.tab}"]`).classList.add('active-service-item-article');
});


// <!-- Our Amazing Work -->

const array = document.querySelectorAll('.works-item');
document.querySelector('.works-filter-wrap').addEventListener('click', (event) => {
    document.querySelector('.works-filter-item.works-filter-item-active').classList.remove('works-filter-item-active');
    event.target.classList.add('works-filter-item-active');
    const newArray = document.querySelectorAll(`.works-gallery li[data-filter = "${event.target.dataset.filter}"]`);
    if (event.target.dataset.filter !== "all") {
        array.forEach(Element =>
            Element.style.display = "none"
        );
        newArray.forEach(Element => {
            if (Element.classList.contains('hidden-works-item')) {
                Element.style.display = "none";
            } else {
                Element.style.display = "block"
            }
        });
    } else if (event.target.dataset.filter === "all") {
        array.forEach(Element => {
            if (Element.classList.contains('hidden-works-item')) {
                Element.style.display = "none";
            } else {
                Element.style.display = "block"
            }
        });
    }
});

const loadMoreButton = document.getElementById('loadMoreBtn');
const loader = document.getElementById('loader');

loadMoreButton.addEventListener('click', () => {
    loadMoreButton.style.display = "none";
    loader.style.display = "flex";
    loader.style.justifyContent = "center";
    setTimeout(() => {
        loader.style.display = "none"
        document.querySelectorAll('.hidden-works-item').forEach(Element =>
            Element.classList.remove('hidden-works-item'));
        const activeFilter = document.querySelector('.works-filter-item-active');
        document.querySelectorAll(`.works-gallery li[data-filter = "${activeFilter.dataset.filter}"]`).forEach(Element =>
            Element.style.display = "block");
    }, 2000)
});

// <!-- What People Say About theHam -->

const carouselGallery = Array.from(document.querySelectorAll('.carousel-item'));
const prevBtn = document.getElementById('prevBtn');

prevBtn.addEventListener('click', () => {
    let activeItem = document.querySelector('.active-carousel-item');
    let index = carouselGallery.indexOf(activeItem);
    if (index === 0) {
        index = 4;
    }
    const newIndex = index - 1;
    activeItem.classList.remove('active-carousel-item');
    document.querySelector('.active-feedback-item').classList.remove('active-feedback-item');
    activeItem = carouselGallery[newIndex];
    document.querySelector(`.feedback-list li[data-user = ${activeItem.dataset.user}]`).classList.add('active-feedback-item');
    carouselGallery[newIndex].classList.add('active-carousel-item');
});

const nextBtn = document.getElementById('nextBtn');

nextBtn.addEventListener('click', () => {
    let activeItem = document.querySelector('.active-carousel-item');
    let index = carouselGallery.indexOf(activeItem);
    if (index === 3) {
        index = -1;
    }
    const newIndex = index + 1;
    activeItem.classList.remove('active-carousel-item');
    document.querySelector('.active-feedback-item ').classList.remove('active-feedback-item');
    activeItem = carouselGallery[newIndex];
    document.querySelector(`.feedback-list li[data-user = ${activeItem.dataset.user}]`).classList.add('active-feedback-item');
    carouselGallery[newIndex].classList.add('active-carousel-item');
});


document.querySelector('.carousel-list').addEventListener('click', (event) => {
    if (event.target.dataset.user) {
        document.querySelector('.carousel-item.active-carousel-item').classList.remove('active-carousel-item');
        document.querySelector(`.carousel-list li[data-user = "${event.target.dataset.user}"]`).classList.add('active-carousel-item');
        document.querySelector('.feedback-item.active-feedback-item').classList.remove('active-feedback-item');
        document.querySelector(`.feedback-list li[data-user = "${event.target.dataset.user}"]`).classList.add('active-feedback-item');
    }
})
