// Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
// Почему объявлять переменную через var считается плохим тоном?

// Значение переменной const задается один раз и не меняется в течение работы программы. Т.е. изменить её значение мы не можем.
// Значение переменной let можно менять. К примеру, используя её мы можем делать какие-то вычисления, в итоге которых мы присвоим let примет
// новое значение.
// Переменная var является устаревшим оператором. 
// var и let отличаются областью видимости. т.е. где данные переменные доступны к использованию. Var может иметь глобальную или 
//локальную область видимости.Глобальная - это когда переменная объявлена вне какой-либо функции (доступна в любой части кода), 
//локальная - когда объявлена внутри функции (доступна только внутри самой функции). let же имеет блочную область видимости (использование
//вне блока, в котором она объявлена приводит к ошибке).




let userName = prompt('Your name');
while (userName === '' || userName === null) {
    userName = prompt('Your name again');
}

let userAge = prompt('Your age');
while (Number.isNaN(userAge) || userAge === null || userAge === '') {
    userAge = prompt('Your age again');
}

if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge >= 18 && userAge <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert(`Welcome,${userName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome,${userName}`);
}


