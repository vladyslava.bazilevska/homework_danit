// Теоретический вопрос
// Опишите своими словами, как Вы понимаете, что такое обработчик событий.
//Обработчик событий - это функция, которая даёт возможность создавать "реакцию" программы на действия пользователя, к примеру - при клике мышкой должно произойти..и прописываем в обработчике, что же именно должно происходить.

// Технические требования:
// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
// В папке img лежат примеры реализации поля ввода и создающегося span.

let input = document.getElementById('input');

function showMessage() {
    let inputValue = document.getElementById('input').value;
    if (inputValue < 0 || !inputValue ) {
        document.body.insertAdjacentHTML("beforeend", `<span class = "error-message">Please enter correct price.</span>`)
        input.style.borderColor= "red";
        input.style.color = "red";

    } else {
    document.body.insertAdjacentHTML("afterbegin",
        `<span class="message">Текущая цена: ${inputValue} <span id = "deleteBtn" onclick="removeListItem(this)"> X</span></span>`);
        input.style.borderColor= "green";
        input.style.color = "green";
    }
}
input.addEventListener('blur', showMessage);

function removeListItem(element) {
    element.closest(".message").remove();
    input.value = '';
}







