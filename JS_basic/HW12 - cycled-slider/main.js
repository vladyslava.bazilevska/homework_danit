// 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
//функция `setTimeout()` отрабатывает один раз через указанный промежуток времени.
//функция `setInterval()` будет повторяться, пока мы не остановим её с помощью clearInterval.

// 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
//данная функция сработает после обработки текущего кода. Т.е. планировщик вызовет данную функцию только после обработки всего кода.

// 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
//Так как данная функция не перестанет вызываться до того момента, когда мы её "отключим", что само по себе нагружает программу и может привести к ошибкам.

const images = document.querySelectorAll('.image-to-show');
const imagesArray = Array.from(images);
const stopButton = document.querySelector('.stop-btn');

function changeImg(interval, i) {
    const timerId = setInterval(() => {
        if (i === imagesArray.length) {
            i = 0;
        }
        document.querySelector('.active-img').classList.remove('active-img');
        const img = imagesArray[i];
        img.classList.add('active-img')
        i++;
    }, interval)
    stopButton.addEventListener('click', () => {
        clearTimeout(timerId);
    })
}

changeImg(3000, 0)

const continueButton = document.querySelector('.continue-btn');
continueButton.addEventListener('click', () => {
    let newIndex;
    imagesArray.forEach((element, key) => {
        if (element.classList.contains('active-img')) {
            newIndex = key;
        }
    })
    changeImg(3000, newIndex + 1)
})
