document.querySelector('.tabs').addEventListener('click', (event) => {
    document.querySelector('.tabs-title.active').classList.remove('active');
    event.target.classList.add('active');
    document.querySelector('.tabs-content .active').classList.remove('active');
    document.querySelector(`.tabs-content li[data-tab = "${event.target.dataset.tab}"]`).classList.add ('active');
})