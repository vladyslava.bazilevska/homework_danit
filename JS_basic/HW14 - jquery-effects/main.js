$(document).ready(function () {
  $('#navigation-menu').on("click", "a", function (event) {
    event.preventDefault();
    const id = $(this).attr('href');
    const top = $(id).offset().top;
    $('body,html').animate({ scrollTop: top }, 1500);
  })
})

const scrollToTopBtn = $('#scrollButton');

$(document).ready(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > $(window).height()) {
      scrollToTopBtn.fadeIn();
    } else {
      scrollToTopBtn.fadeOut();
    }
  });
});

scrollToTopBtn.on('click', function () {
  $('body, html').animate({
    scrollTop: 0
  }, 1500);
})

$('#slideTogglePopularPosts').click(function () {
  $('.gallery-list').slideToggle("slow");
})