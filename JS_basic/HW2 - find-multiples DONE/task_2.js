// Необязательное задание продвинутой сложности:
// Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять вывод окна на экран до тех пор, пока не будет введено целое число.
// Считать два числа, m и n. Вывести в консоль все простые числа в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше, вывести сообщение об ошибке, и спросить оба числа заново.


let m = +prompt('Your number m');
while (!Number.isInteger(m)) {
    m = prompt ('Your number again m');
}

let n = +prompt('Your number n');
while (!Number.isInteger(n)) {
    n = prompt ('Your number again N');
}

while (m >= n) {
    m = prompt ('Your number again M');
    n = prompt ('Your number again N');
}

next: 
for (let i = m; i < n; i++) {
    for (let a = 2; a < i ; a++) {
        if ( i % a == 0 ) continue next;
    }
    console.log (i);
}
