
const mainPage = document.getElementById('mainPage');


window.onload = function () {
    if (localStorage.getItem('theme') === 'dark') {
        mainPage.setAttribute('data-theme', 'dark');
    }
}
document.getElementById('themeButton').addEventListener('click', function () {
    if (localStorage.getItem('theme') === 'dark') {
        mainPage.removeAttribute('data-theme', 'dark');
        localStorage.removeItem('theme', 'dark');
    } else {
        mainPage.setAttribute('data-theme', 'dark')
        localStorage.setItem('theme', 'dark');
    }
})
