// Теоретический вопрос
// Опишите своими словами, что такое метод обьекта
//Метод объекта дает нам возможность работать с полученными в процессе работы программы данными (свойствами объекта), т.е. мы прописываем к примеру функцию при помощи которой в дальнейшем она формирует новое свойство исходя из заданных нами в функции условий. Это даёт нам возможно не только получать и записывать свойства, но и работать с ними внутри объекта.

function createNewUser(firstName, lastName) {
    const newUser = {
        firstName,
        lastName,
        getLogin: function () {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        }
    } //защитили свойства от перезаписи!
    Object.defineProperty(newUser, 'firstName', {
        writable: false,
    })
    Object.defineProperty(newUser, 'lastName', {
        writable: false,
    })
    return newUser;
}

let userProfile = createNewUser(prompt('Your name'), prompt('Your lastname'));
console.log(userProfile.getLogin());



//Вариант с confirm.
// let createLogin = confirm('Do you want to create login?');

// if (createLogin) {
//     console.log(userProfile.getLogin());
// } else {
//     console.log('Goodbye!')
// }
