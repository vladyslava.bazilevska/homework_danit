// Теоретический вопрос
// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
//Экранирование необходимо для использования символов и групп символов в контексте программы и избежания их влияния на саму работу программы.


// Задание
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.



function createNewUser(firstName, lastName, birthday) {
    const newUser = {
        firstName,
        lastName,
        birthday, //23.03.1992
        getLogin: function () {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
        getAge: function () {
            return new Date ().getFullYear() - birthday.slice(6, 10);
        },
        getPassword: function () {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(6, 10)}`
        }
    }
    Object.defineProperty(newUser, 'firstName', {
        writable: false,
    })
    Object.defineProperty(newUser, 'lastName', {
        writable: false,
    })
    return newUser;
}

let userProfile = createNewUser(prompt('Your name'), prompt('Your lastname'), prompt('Your bithday (dd.mm.yyyy)'));
console.log(createNewUser());
console.log(userProfile.getAge());
console.log(userProfile.getPassword());
