// Почему для работы с input не рекомендуется использовать события клавиатуры?
// Не рекомендуется, так как помимо набора пользователь может также надиктовывать текст, или же вставить при помощи мыши и т.д.


const array = document.querySelectorAll('.btn');
document.addEventListener('keydown', function (event) {
	array.forEach(element => {
		if (element.dataset.key === event.code) {
			element.classList.add('btn-active');
		} else {
			element.classList.remove('btn-active');
		}
	});
})
