// Теоретический вопрос
// Описать своими словами для чего вообще нужны функции в программировании.
// При написании программы одной из основных задач является её оптимизация, т.е. при помощи наименьшего кол-ва строк и символов,
// сделать её рабочей и логичной. В основном многие действия в программе повторяются в разных чатсях кода (например проверка 
//валидности значений), чтобы не дублировать инструкции - созданы функции.

// Описать своими словами, зачем в функцию передавать аргумент.
//Передача аргументов дает возможность работать с функцией в разных частях кода с разными значениями. Т.е. например мы получили 
//новые данные от юзера и нам нужно их проверить (у нас уже есть функция валидации), мы передаем эти аргументы функции, чтобы 
//произвести действие прописанное в ней.


let firstNumber = prompt('Your first number');
while (!validNumber(firstNumber)) {
    firstNumber = prompt('Your first number again');
}

let secondNumber = prompt('Your second number');
while (!validNumber(secondNumber)) {
    secondNumber = prompt('Your second number again');
}

function validNumber(a) {
    if (Number.isNaN(a) || a === '' || a === null) {
        return false
    } else {
        return true
    }
}

let action = prompt('What do you what to do? (+, -, *, /)')

function multiplication(a, b) {
    return a * b;
}

function division(a, b) {
    return a / b;
}

function subtraction(a, b) {
    return a - b;
}

function sum(a, b) {
    return a + b;
}

function calculate(x, y, operation) {
    if (operation === '*') {
        return multiplication(x, y);
    } else if (operation === '/') {
        return division(x, y);
    } else if (operation === '-') {
        return subtraction(x, y);
    } else if (operation === '+') {
        return sum(x, y);
    } else {
        return 'It is a bad idea';
    }
}

console.log (calculate(+firstNumber, +secondNumber, action));

