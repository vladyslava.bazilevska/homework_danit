// Теоретический вопрос
// Опишите своими словами как работает цикл forEach.
//Цикл forEach в массивах создан для перебора каждого элемента массива. Во время перебора мы можем обратиться к каждому элементу и совершить с них необходимое нам действие.

// Задание
// Реализовать функцию фильтра массива по указанному типу данных. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].


function filterBy (array, type) {
    const newArray = array.filter (element => typeof element != type);
    console.log (newArray)
}

let arr = ['hello', 'world', 23, '23', null] ;
let filter = ('string');
filterBy (arr, filter)

