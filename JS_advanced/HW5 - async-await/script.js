// Обьясните своими словами, как вы понимаете асинхронность в Javascript
// в Javascript нет многопоточности. Т.е. мы не можем выполнять сразу несколько действий с одиними и теми же элементами. Операции идут
// по очереди. Часто нам необходимо контролировать эту очередность, т.е. например мы ждём ответа от сервера, после чего делаем
// что-то с полученными данными. В таком случае нам необходимо быть уверенными в том что, написанная нами задача (функция)
// будет отработана только после получения данных. Именно для это мы используем асинхронные операции, тем самым "говорим"
// JS очередность выполнения задач.


const searchButton = document.getElementById('searchBtn')

searchButton.addEventListener('click', getUserIp)

async function getUserIp() {
    await fetch('https://api.ipify.org/?format=json')
        .then(response => response.json())
        .then(data => { getUserAdress(data)})
}

async function getUserAdress(data) {
    const { ip } = data
    await fetch(`http://ip-api.com/json/${ip}`)
        .then(response => response.json())
        .then(data => createInfoCard(data))
}

function createInfoCard(data) {
    const { city, country, region, regionName, timezone } = data;
    searchButton.insertAdjacentHTML('afterend', `
    <div class="infoCard">
    <p>Континент: ${timezone}</p>
    <p>Страна: ${country}</p>
    <p>Регион: ${regionName}</p>
    <p>Город: ${city}</p>
    <p>Район города: ${region}</p>
    </div>
    `)
}