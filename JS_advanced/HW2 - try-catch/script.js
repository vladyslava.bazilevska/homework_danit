// Приведите пару примеров, когда уместно использовать в коде конструкцию `try...catch`.
//часто используется при работе с сервером. Т.е. если мы понимаем, что возможно запрос на сервер вернёт нам ошибку - целесообразно
//использовать данную конструкцию, чтобы программа продолжила свою работу даже в случае полученной ошибки.

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const neccessaryProperties = ['name', 'author', 'price']

const renderBooks = (books) => {
    books.forEach((book) => {
        try {
            const [resultOfValidation, array] = validationFunction(book);
            if (resultOfValidation) {
                createList(array);
            } else {
                throw new NoPropertyError(array)
            }
        } catch (error) {
            console.log(error);
        }
    })
};

function validationFunction(book) {
    let resultOfValidation = false;
    let array = [];
    let booksProperties = Object.getOwnPropertyNames(book);
    if (booksProperties.length === neccessaryProperties.length) {
        array = new Array(book);
        resultOfValidation = true;
    } else {
        neccessaryProperties.forEach(function (element) {
            if (!~booksProperties.indexOf(element)) array.push(element);
        });
    }
    return [resultOfValidation, array]
};

function createList(array, parent = document.getElementById('root')) {
    const list = array.map(element => {
        return `<li>${element.author}, "${element.name}", ${element.price} грн </li>`
    }).join('');
    parent.insertAdjacentHTML('afterbegin', `<ul>${list}</ul>`);
};

class NoPropertyError extends Error {
    constructor(property) {
        super();
        this.name = "NoPropertyError";
        this.message = `No property ${property}`;
        this.property = property;
    }
};

renderBooks(books);