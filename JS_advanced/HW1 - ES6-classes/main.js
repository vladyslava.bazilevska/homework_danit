// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

// Прототипное наследование в JS даёт возможность создавать объекты на основе прототипов (ранее созданных нами объектов). 
// Т.е. когда прототипом нашего объекта является другой объект, он унаследует все его свойства и методы. Мы можем
// как использовать их не перезаписывая, так и менять их по мере необходимости. Данный подход даёт нам возможность не дублировать код,
// что значительно сокращает время разработки, так как зачастую объекты в одной программе схожи.
// Также помимо этого у базового прототипа Object также есть свойства и методы, которые всегда являются основой нашего объекта и дают нам возможность использовать
// встроенный функционал.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(name) {
        this._name = name;
    }
    get age() {
        return this._age;
    }
    set age(age) {
        this._age = age;
    }
    get salary() {
        return this._salary;
    }
    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

const firstProgrammer = new Programmer('John', 23, 2300, ['english', 'russian']);
const secondProgrammer = new Programmer('James', 35, 1300, ['english', 'deutsch']);
const thirdProgrammer = new Programmer('David', 43, 3600, ['deutsch', 'spanish']);

console.log(firstProgrammer);
console.log(secondProgrammer);
console.log(thirdProgrammer);