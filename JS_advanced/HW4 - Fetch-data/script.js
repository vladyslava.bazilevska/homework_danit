// ## Задание
// Получить список фильмов серии `Звездные войны`, и вывести на экран список персонажей для каждого из них.

// #### Технические требования:
// - Отправить AJAX запрос по адресу `https://ajax.test-danit.com/api/swapi/films` и получить список всех фильмов серии `Звездные войны`
// - Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства `characters`.
// - Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля `episode_id`, `title` и `opening_crawl`).
// - Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.

// #### Необязательное задание продвинутой сложности
//  - Пока загружаются персонажи фильма, прокручивать под именем фильма анимацию загрузки. Анимацию можно использовать любую. Желательно найти вариант на чистом CSS без использования JavaScript.


fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(response => response.json())
    .then(data => shortFilmInfo(data))
    .then(result => createList(result))
    .then(list => renderCharacters(list))

function shortFilmInfo(array) {
    let shortFilmInfoList = array.map(({ episodeId, name, openingCrawl, characters }) => ({
        episodeId,
        name,
        openingCrawl,
        characters
    }));
    return shortFilmInfoList;
}


function createList(list) {
    let newElement = document.createElement('ul');
    newElement.classList.add('list')
    document.getElementById('filmListWrap').insertAdjacentElement('afterbegin', newElement);
    list.forEach(element => {
        const { episodeId, name, openingCrawl } = element;
        let list = document.querySelector(".list")
        list.insertAdjacentHTML('afterbegin', `
        <li><h2>Episode number: ${episodeId}</h2>
        <h3 id=${episodeId}> Movie title: ${name}</h3>
        <p><b>Summary: </b> ${openingCrawl}</p></li>`);
    });
    return list;
}

 function renderCharacters(array) {
    array.forEach(element => {
        const { episodeId, characters } = element;
        characters.forEach(element => {
            fetch(element)
                .then(response => response.json())
                .then(data => data.name)
                .then((result) => {
                    document.getElementById(`${episodeId}`).insertAdjacentHTML("afterend", `${result} `)
                })
        })
    })
    return array;
}

