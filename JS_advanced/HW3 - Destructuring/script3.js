// ### Задание 3

// У нас есть объект `user`:

// Напишите деструктурирующее присваивание, которое:
//  - свойство name присвоит в переменную name
//  - свойство years присвоит в переменную age
//  - свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
 
// Выведите переменные на экран.

const user1 = {
    name: "John",
    years: 30
  };

const {name,  years: age, isAdmin = false} = user1;

function createElement (name, age, isAdmin) {
  let newElement = document.createElement("div");
  newElement.innerHTML = `His name is ${name}, he is ${age} years old. Is he an admin? - ${isAdmin}.`
  document.body.insertAdjacentElement('afterbegin', newElement)
}

createElement(name, age, isAdmin);