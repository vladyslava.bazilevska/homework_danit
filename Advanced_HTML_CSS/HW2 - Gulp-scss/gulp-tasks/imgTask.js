
const gulp = require('gulp');
const image = require('gulp-image');

function imgMin() {
    return gulp.src('src/img/**')
        .pipe(image())
        .pipe(gulp.dest('dist/img'));
}

exports.img = imgMin;
