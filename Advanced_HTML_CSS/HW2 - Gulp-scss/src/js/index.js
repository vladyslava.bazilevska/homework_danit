const mainMenuButton = document.getElementById('mainMenuButton');
const mainMenuButtonImg = document.getElementById('mainMenuButtonImg');

mainMenuButton.addEventListener('click', () => {
    if (mainMenuButton.classList.contains('active-menu-btn')) {
        mainMenuButton.classList.remove('active-menu-btn');
        mainMenuButtonImg.src = "./img/burger-btn.svg";
        document.querySelector('.header-container__nav').classList.add('hidden-nav-menu');
    } else {
        mainMenuButton.classList.add('active-menu-btn');
        mainMenuButtonImg.src = "./img/menu-button.svg";
        document.querySelector('.header-container__nav').classList.remove('hidden-nav-menu');
    }
})

